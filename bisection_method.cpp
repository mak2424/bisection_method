#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>

/* Вставка элементов в массив и сортировка налету методом деления отрезка пополам */

using namespace std;
int main(int argc, char** argv) {
    
    srand(time(0));
    
    vector<int> randVec;
    vector<int> randVecCount;
    
    int randVal = rand()%10; //задаем случайное число
    
    int znach = 1; //указываем на какое значение увеличивать
    
    cout << "randVal_1 = " << randVal << endl;
    
    randVec.push_back(randVal);
    randVecCount.push_back(znach);
    
    //цикл добавления числа
    for (int i=0; i<9; ++i)
    {
        randVal = rand()%10;
        cout << "randVal_" << i+2 << " = " << randVal << endl;
        
        int a = 0; //задаем начало отрезка
        int b = randVec.size()-1; //конец отрезка
        
        //1) делим массив на отрезки до тех пор, пока не останется отрезок из 1го или 2х элементов
        while (b-a+1 > 2)
        {
            int half = (b+a)/2;
            if(randVal <= randVec[half])
                b = half;
            else
                a = half+1;
        }
        
        //2) проверка единичного отрезка
        if ((b-a)+1 == 1)
        {
            if (randVal < randVec[a])
            {
                randVec.insert(randVec.begin()+a, randVal);
                randVecCount.insert(randVecCount.begin()+a, znach);
            }
            else
                if (randVal > randVec[a])
                {
                    randVec.insert(randVec.begin()+b+1, randVal);
                    randVecCount.insert(randVecCount.begin()+b+1, znach);
                }
                else
                    if (randVal == randVec[a])
                    {
                        randVecCount[a]+=znach;
                    }
        }
        
        //3) проверка отрезка из 2х элементов
        else{
            if ((b-a)+1 > 2)
            {
                cout << "Error! Size = " << (b-a)+1 << endl;  //проверка на случай, если не отрезок больше 2х элементов
            }
            
            if (randVal < randVec[a])
            {
                randVec.insert(randVec.begin()+a, randVal);
                randVecCount.insert(randVecCount.begin()+a, znach);
            }
            
            else
                if(randVal == randVec[a])
                {
                    randVecCount[a]+=znach;
                }
            
                else
                    if (randVal > randVec[b])
                    {
                        randVec.insert(randVec.begin()+b+1, randVal);
                        randVecCount.insert(randVecCount.begin()+b+1, znach);
                    }
            
                    else
                        if(randVal == randVec[b])
                        {
                            randVecCount[b]+=znach;
                        }
            
                        else
                            if(randVal > randVec[a] && randVal < randVec[b])
                            {
                                randVec.insert(randVec.begin()+b, randVal);
                                randVecCount.insert(randVecCount.begin()+b, znach);
                            }
        }
    }
    
    cout << endl;
    
    for(int j = 0; j<randVec.size(); ++j)
    {
        cout << "randVec[" << j <<"] = " << randVec[j] << " - " << randVecCount[j] << endl;
    }
    
    cout << "\nTotal Size Of Vector = " << randVec.size() << endl << endl;
    
    for(int i = 1; i<randVecCount.size(); ++i)
    {
        randVecCount[0] +=randVecCount[i];
    }
    
    cout << "Total values = " << randVecCount[0] << endl;
    
    return 0;
}
